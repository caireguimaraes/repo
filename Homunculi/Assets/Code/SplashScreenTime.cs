﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SplashScreenTime : MonoBehaviour
{
    private Scene scene;
    private int sceneIndex;
    public int splashScreenTime;
    // Start is called before the first frame update
    void Start()
    {
        scene = SceneManager.GetActiveScene();
        sceneIndex = scene.buildIndex;
        StartCoroutine(TimeToLoadNextScene());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator TimeToLoadNextScene()
    {
        yield return new WaitForSecondsRealtime(splashScreenTime);
        SceneManager.LoadScene(sceneIndex + 1);
    }
}
